/**
* A classe App.class implementa o programa solicitado no enunciado para exercício de recrutamento para o Gabinete de Desenvolvimento
*de Sistemas de Informação do Iscte – Instituto Universitário de Lisboa.
*
*O programa solicita um endereço ao utilizador no formato pedido no enunciado.
*  Se o endereço estiver no formato correto, o programa devolve as referências contidas no artigo da Wikipedia correspondente
*  Se o endereço estiver no formato correto, mas o artigo não for encontrado, o programa devolve uma mensagem de erro e termina
*  Se o endereço não estiver no formato correto, é emitido um alerta e selecionado um artigo aleatoriamente, sendo devolvidas
*  as respetivas referências
*  
*Utiliza a classe WikipediaDocument, criada para lidar com o documento HTML extraído do site da Wikipedia. 
*A classe App lida com as referências propriamente ditas (texto)
*
*
* @author  Filipe Nunes
* @version 1.0
* @since   2021-12-30
*/


package pt.iscte_iul.gdsi;

import java.util.*;



public class App
{
   //Declaração de constantes
	private static final String BASE_URL = "https://en.wikipedia.org/wiki/";
	private static final String RANDOM_URL = BASE_URL + "Special:Random";
	

	public static void main( String[] args )
    {
		//bibliography - lista de referências bibliográficas em formato texto
		ArrayList<String> bibliography = new ArrayList<String>();
		int i=1;
		
		//doc - o documento HTML
		WikipediaDocument doc = null;
		
		//ddress - endereço da página a introduzir pelo utilizador
		String address = "";
		
		//ler endereço da consola
		Scanner s = new Scanner(System.in);			
		System.out.println(String.format("Introduza um endereço no formato: '%s<Artigo>'", BASE_URL));
		address = s.nextLine();
		s.close();
		
		//Validação do endereço e construção do documento HTML
		if (isValidFormat(address)) {
			doc = new WikipediaDocument(address);
		} else {
			//Formato inválido - seleciona aleatoriamente outro endereço
			System.out.println("O endereço não é válido. A selecionar um artigo aleatoriamente...");
			doc = new WikipediaDocument(RANDOM_URL);
		}
		
		//se o documento estiver vazio, o artigo não foi encontrado
		//pode acontecer caso o nome do artigo esteja mal escrito ou se houverem problemas de rede
		if (doc.isEmpty()) {
			System.out.println("Artigo não encontrado");
			return;
		}
		
		//obter referências do documento
        bibliography = doc.getRefs();
       
      
	    System.out.println();
	    System.out.println(doc.getName());
	    System.out.println("====================");
	    System.out.println();
	    System.out.println("References:");
	    System.out.println();
	    
	    //se a lista de referências estiver vazia
	    if (Objects.isNull(bibliography)) {
    		System.out.println("Artigo sem referências.");
    		return;
    	} 
            
	    //Exibir as referências na consola
	    for (String el:bibliography) {
    	    System.out.println(i + ". " + el);
    	    i++;
	    }
       
	}
	
	//Método para validar o formato do endereço introduzido pelo utilizador
	private static boolean isValidFormat(String url) {
		
		if (url.startsWith(BASE_URL) ) {
			return true;
		} else {
			return false;
		}
	}
}

	
	
	
	
	

