/**
* A classe WikipediaDocument é parte integrante do programa solicitado no enunciado para exercício de recrutamento para o 
* Gabinete de Desenvolvimento de Sistemas de Informação do Iscte – Instituto Universitário de Lisboa.
*tiliza a biblioteca de classes JSoup, que facilita a manipulação dos elementos HTML do documento extraído do site da Wikipedia
*
*
* @author  Filipe Nunes
* @version 1.0
* @since   2021-12-30
*/


package pt.iscte_iul.gdsi;

import java.io.*;
import java.util.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;


public class WikipediaDocument {
	
	//doc - a secção body do documento
	private Element doc;
		
	//Construtor
	public WikipediaDocument (String article) {
		
		try {
				doc = Jsoup.connect(article).get();
			
		} catch (IOException e) {
			//problemas de rede
			doc = null;
		
		} catch (IllegalArgumentException e) {
			//Artigo inexistente
			doc = null;
		} 

	}
	
//Método que devolve o nome do artigo
public String getName() {

	if(Objects.isNull(doc)) {
		return null;
	} else {
		//conteúdo da tag <title> do documento
		return doc.select("title").first().text();
	}
}


//Método que devolve as referências bibliográficas contidas no documento
//como uma lista de strings
public ArrayList <String> getRefs() {
	
	//refs - lista de referências bibliográficas em formato texto
	ArrayList<String> refs = new ArrayList<String>();
	
	//biblioRefs - lista de referências bibliográficas no formato Element (JSoup)
	Elements biblioRefs = new Elements();

	//bibliography - objeto Element auxiliar	
	Element bibliography;
	
	//seleção do elemento seguinte ao h2 que identifica as referências, se existirem
	if(Objects.isNull(doc) || doc.select("h2:has(span[id=References])").text().isBlank()) {
		return null;
	} else {
		bibliography = doc.select("h2:has(span[id=References])").first().nextElementSibling();
	}
	
	//Há artigos que contêm mais que uma lista de referências
	//O ciclo do - while considera todas as listas
		
	do {
	
		//enquanto o elemento selecionado não for uma lista de referências seleciona-se o elemento seguinte até se encontrar
		while (!endOfRefs(bibliography) && bibliography.select("ol[class=references], ul").size()==0) {
			bibliography = bibliography.nextElementSibling();
		}
		
		//Se não se encontrarem mais referências, termina-se o ciclo
		if (endOfRefs(bibliography)) {
			break;
		}
		//Dados os critérios de seleção (cssQuery) usados, pode acontecer que a mesma referência
		//seja adicionada 2 vezes à lista.
		//Torna-se necessário eliminar as referências duplicadas
		biblioRefs.addAll(removeDuplicates(bibliography.select("ul > li, span[class$=text], span[class^=text], cite")));
		
		//Procurar a lista seguinte, se existir
		bibliography = bibliography.nextElementSibling();

	} while (!endOfRefs(bibliography));


	
	//Converter as referências para texto
	for (Element el:biblioRefs) {
		refs.add(el.text());
	}
	
	//devolver a lista de strings
	return refs;
		 
}


//Método que verifica se se chegou ao fim das referências
//Assume-se que a secção de referências termina no primeiro <h2>, <p>, 
//«<div class=\"navbox» ou <table>após a/as lista/s dereferências
private boolean endOfRefs(Element refs) {
	
	return
	Objects.isNull(refs) ||
	refs.outerHtml().startsWith("<h2") || 
	refs.outerHtml().startsWith("<p") ||
	refs.outerHtml().startsWith("<div class=\"navbox") ||
	refs.outerHtml().startsWith("<table");
	
}

//Método para remover elementos duplicados. Recebe uma lista de referências
private Elements removeDuplicates(Elements refsWithDuplicates) {
	
	//refsWithNoDuplicates - a lista final a devolver, sem repetições
	Elements refsWithNoDuplicates = new Elements();
	String aux;
	
	if (Objects.isNull(refsWithDuplicates)) {
		return null;
	} else if (refsWithDuplicates.size() <= 1) {
		//se o tamanho da lista recebida for <= 1, não há duplicados
		return refsWithDuplicates;
	} else {
		
		//adiciona o primeiro elemento da lista inicial à lista final
		refsWithNoDuplicates.add(refsWithDuplicates.get(0));
		
		//percorrer a lista
		for (int i =0; refsWithDuplicates.size() >= i+1; i++) {
			//obtem o último elemento da lista final
			aux = refsWithNoDuplicates.get(refsWithNoDuplicates.size()-1).text();
			
			//se o último elemento da lista final contiver o elemento seguinte da lista inicial, considera-se duplicado e não se adiciona
			//Um elemento duplicado pode ser igual ou estar contido no anterior, salvaguardando os casos em que as referências têm
			//sub-listas
			if (!aux.contains(refsWithDuplicates.get(i).text())) {
				refsWithNoDuplicates.add(refsWithDuplicates.get(i));
			}
		}
	}
	
	//devolução da lista limpa
	return refsWithNoDuplicates;
	
}

//método que testa se o documento está vazio
public boolean isEmpty() {
	if (Objects.isNull(doc)) {
		return true;
	} else {
		return false;
	}
}

}